<?php include("db.php") ?>

<?php include("includes/encabezado.php") ?>


<div class="container p-4">

    <div class="row">

        <div class="col-md-8">

            <?php if(isset($_SESSION['message'])) { ?>
            <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
                <?= $_SESSION['message'] ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </div>
            <?php  session_unset();} ?>
            <form action="save_clientes.php" method="POST">

                <h2 class="display-5">¡Hola! Ingresa tus datos</h2>
                <div class="form-group">
                    <div class="col-md-4 mb-3">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Ingrese su nombre" autofocus
                            required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="apellido">Apellido</label>
                        <input type="text" name="apellido" class="form-control" placeholder="Ingrese su apellido"
                            required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 mb-3">
                        <label for="telefono">Telefono</label>
                        <input type="text" name="telefono" class="form-control" placeholder="Ingrese su telefono"
                            required>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="dni">DNI</label>
                        <input type="text" name=" dni" class="form-control" placeholder="Ingrese su DNI" required>
                    </div>
                </div>

                <button class="btn btn-primary" type="submit" name="enviar">Enviar</button>
            </form>
        </div>
    </div>



    <?php include("includes/body.php")?>