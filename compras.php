<?php include("db.php") ?>

<?php include("includes/encabezado.php") ?>


<div class="container p-4">

    <div class="row">

        <div class="col-md-8">

            <?php if(isset($_SESSION['message'])) { ?>
            <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
                <?= $_SESSION['message'] ?>


                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php  session_unset();} ?>

            <form action="save_clientes.php" method="POST">
                <h2>Realice su venta</h2>
                <div class="container">
                    <select name="id_cliente" id="" required>
                        <?php
                        
                        $sql = $conexion -> query("SELECT * FROM cliente");
                        
                        while($fila=$sql->fetch_array()){
                            echo "<option value='".$fila['id_cliente']."'>".$fila['nombre']."</option>";
                        
                        }
                        ?>
                    </select>


                    <select name="id_producto" id="producto" required>
                        <?php
                        
                        $sql = $conexion -> query("SELECT * FROM producto");
                        
                        while($fila=$sql->fetch_array()){
                            echo "<option value='".$fila['id_producto']."'>".$fila['nombre_producto']."</option>";
                        
                        }
                        ?>
                    </select>


                    <button class="btn btn-primary" type="submit" name="enviar_compra">Enviar</button>

                </div>

            </form>

        </div>
    </div>

    <?php include("includes/body.php")?>