<?php include("includes/encabezado.php")?>

<?php include("db.php")?>



<!-- PRODUCTOS  -->
<div class="container p-4">

    <div class="row">

        <div class="col-md-8">

            <?php if(isset($_SESSION['message'])) { ?>
            <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
                <?= $_SESSION['message'] ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php  session_unset();} ?>
            <form action="save_clientes.php" method="POST">
                <h2 class="display-5">¿Qué producto desea registrar?</h2>
                <div class="form-group">
                    <div class="col-md-4 mb-3">
                        <label for="nombre_producto">Producto</label>
                        <input type="text" name="nombre_producto" class="form-control"
                            placeholder="Ingrese el nombre de su producto" required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="cantidad">Cantidad</label>
                        <input type="number" name="cantidad" class="form-control" placeholder="Ingrese la cantidad"
                            required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 mb-4">
                        <label for="precio">Precio</label>
                        <input type="number" step="any" name="precio" class="form-control"
                            placeholder="Ingrese el precio unitario" required>
                    </div>
                    <button class="btn btn-primary" type="submit" name="enviar2">Enviar</button>
                    <h3>
                        <h4>
                            <h2></h2>
                        </h4>
                    </h3>
                </div>

                <table class="table table-success table-striped">
                    <thead class="table-dark">

                        <th> Producto </th>
                        <th>Cantidad</th>
                        <th>Precio</th>

                    </thead>
                    <tbody>
                        <?php 
                        $query= "SELECT * FROM producto";
                        $resultado= mysqli_query($conexion,$query);
                        while($row = mysqli_fetch_array($resultado)){  ?>

                        <tr>
                            <td><?php echo $row['nombre_producto'] ?></td>
                            <td><?php echo $row['cantidad'] ?></td>
                            <td><?php echo $row['precio'] ?></td>
                        </tr>

                    </tbody>
                    <?php } ?>
                </table>
        </div>
    </div>
</div>



<?php include("includes/body.php")?>